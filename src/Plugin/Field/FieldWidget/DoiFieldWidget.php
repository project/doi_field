<?php

namespace Drupal\doi_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'doi_field_widget' field widget.
 *
 * @FieldWidget(
 *   id = "doi_field_widget",
 *   label = @Translation("Doi Field"),
 *   field_types = {"doi_field"},
 * )
 */
class DoiFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? NULL,
    ];

    return $element;
  }

}
