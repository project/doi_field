<?php

namespace Drupal\doi_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\doi_search\DoiSearchManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Doi Field' formatter.
 *
 * @FieldFormatter(
 *   id = "doi_field_formatter",
 *   label = @Translation("Doi Field"),
 *   field_types = {
 *     "doi_field"
 *   }
 * )
 */
class DoiFieldFormatter extends FormatterBase {

  /**
   * The Doi Search Manager.
   *
   * @var \Drupal\doi_search\DoiSearchManager
   */
  protected $doiSearchManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    DoiSearchManager $doi_search_manager,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->doiSearchManager = $doi_search_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('doi_search.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'show' => ['title', 'author'],
      'labels' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['show'] = [
      '#type' => 'checkboxes',
      '#options' => [
        'title' => $this->t('Title'),
        'author' => $this->t('Author'),
        'abstract' => $this->t('Abstract'),
        'date' => $this->t('Date'),
        'link' => $this->t('Link'),
        'pdf' => $this->t('PDF Link (if available)'),
      ],
      '#title' => $this->t('Elements to show'),
      '#default_value' => $this->getSetting('show'),
      '#required' => TRUE,
    ];
    $elements['labels'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show elements labels'),
      '#default_value' => $this->getSetting('labels'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $options = [
      'title' => $this->t('Title'),
      'author' => $this->t('Author'),
      'abstract' => $this->t('Abstract'),
      'date' => $this->t('Date'),
      'link' => $this->t('Link'),
      'pdf' => $this->t('PDF Link (if available)'),
    ];
    $selected = $this->getSetting('show');
    $text = [];
    foreach ($selected as $option) {
      if (!empty($options[$option])) {
        $text[] = $options[$option];
      }
    }
    $summary[] = $this->t('Visible Elements: @elements', ['@elements' => implode(',', $text)]);
    $summary[] = $this->getSetting('labels') ? $this->t('Show elements labels') : $this->t('Hide elements labels');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $item) {
      $elements[] = [
        'content' => $this->getInfo($item->value),
        'attributes' => new Attribute([]),
      ];
    }
    $build = [
      '#theme' => 'field__field_doi',
      '#items' => $elements,
      '#multiple' => $this->fieldDefinition->getFieldStorageDefinition()->isMultiple(),
      '#label' => $this->fieldDefinition->getLabel(),
      '#field_name' => $this->fieldDefinition->getName(),
      '#label_hidden' => ($this->label !== 'hidden'),
      '#data_labels' => $this->getSetting('labels'),
    ];
    return $build;
  }

  /**
   * It takes a DOI and returns an array of information about the publication.
   *
   * @param string $doi
   *   The DOI of the publication.
   *
   * @return array
   *   An array of the selected information.
   */
  private function getInfo($doi) {
    $data = $this->doiSearchManager->getData($doi);
    $selected = $this->getSetting('show');
    $info = [];
    foreach ($selected as $option) {
      switch ($option) {
        case 'title':
          $info['title'] = $this->getPublicationTitle($data);
          break;

        case 'author':
          $info['author'] = isset($data->author) ? $this->formatAuthors($data->author) : '';
          break;

        case 'abstract':
          if (isset($data->abstract)) {
            $info['abstract'] = $data->abstract;
          }
          break;

        case 'date':
          $date = isset($data->created) ? (array) $data->created : [];
          $info['date']['text'] = isset($date['date-time']) ? date("d M Y", strtotime($date['date-time'])) : '';
          $info['date']['time'] = $date['date-time'] ?? '';
          break;

        case 'link':
          $info['link'] = $data->URL ?? '';
          break;

        case 'pdf':
          $info['pdf'] = $this->getPdfLink($data);
          break;

        default:
          break;
      }
    }
    return $info;
  }

  /**
   * Loops through the array of data and returns the title of the publication.
   *
   * @param object $data
   *   The data array from the JSON response.
   *
   * @return string
   *   The title of the publication.
   */
  private function getPublicationTitle($data) {
    $title = '';
    foreach ($data as $key => $value) {
      if (strpos($key, "title") !== FALSE && !empty($value) && !(strpos($key, "container") !== FALSE)) {
        $title = is_array($value) ? $value[0] : $value;
        break;
      }
    }
    return $title;
  }

  /**
   * If the link is a PDF, return the link. Otherwise, return NULL.
   *
   * @param object $data
   *   The data object returned from the API.
   *
   * @return string
   *   the pdf link if it exists.
   */
  private function getPdfLink($data) {
    if (isset($data->link)) {
      foreach ($data->link as $link) {
        if (strtolower(substr($link->URL, -3, 3)) == "pdf") {
          $pdf = $link->URL;
          break;
        }
      }
    }
    return $pdf ?? NULL;
  }

  /**
   * It takes an array of authors and returns a string of authors.
   *
   * @param array $authors
   *   The authors of the article.
   * @param string $separator
   *   The string to separate the authors names when displaying.
   *
   * @return string
   *   the formatted authors.
   */
  private function formatAuthors(array $authors, $separator = ', ') {
    $formattedAuthors = [];
    foreach ($authors as $author) {
      $given = isset($author->given) ? $author->given . ' ' : '';
      if (empty($given) && !isset($author->family)) {
        return $author->name ?? '';
      }
      $formattedAuthors[] = $given . $author->family;
    }
    return implode($separator, $formattedAuthors);
  }

}
