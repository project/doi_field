# DOI Field

Provides a Doi type field.

You can configure the display of the field to show the selected elements of
the corresponding publication 

For a full description of the module, visit the
[project page](https://www.drupal.org/project/doi_field).

To submit bug reports and feature suggestions, or to track changes:
[issue queue](https://www.drupal.org/project/issues/doi_field).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

[requires Doi Publications Search](https://www.drupal.org/project/doi_search).


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. When creating a new field on a content type or custom entity type, choose
   "DOI Field" from the drop-down menu.


## Maintainers

Current maintainers:
- Paulo Calado - [kallado](https://www.drupal.org/u/kallado).
- João Mauricio [jmauricio](https://www.drupal.org/u/jmauricio).
  
This project has been sponsored by:

Visit for more information - [Javali](https://www.javali.pt).
